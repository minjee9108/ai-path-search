Agent.java implements a computer agent that plays a simple text-based adventure game. 

# Rules of the game #

The player is required to move around a rectangular environment, collecting tools and avoiding (or removing) obstacles along the way. The aim of the game is to find the gold and return to the starting position. At any step, the player is only shown 5x5 window of the environment centering around the player. The elements within the environment are represented as follows:

**Obstacles**

* T - TREE
* \* - WALL
* ~ - WATER

**Tools**

* a - AXE
* d - DYNAMITE
* B - BOAT

**TARGET**

* g - GOLD
 
The agent will be represented by one of the characters ^, v, <  or  >, depending on which direction it is pointing. The agent is capable of the following actions:

* L   turn left
* R   turn right
* F   (try to) move forward
* C   (try to) chop down a tree, using an axe
* B   (try to) blast a wall or tree, using dynamite

When it executes an L or R instruction, the agent remains in the same location and only its direction changes. When it executes an F instruction, the agent attempts to move a single step in whichever direction it is pointing. The F instruction will fail (have no effect) if there is a wall or tree directly in front of the agent.

When the agent moves to a location occupied by a tool, it automatically picks up the tool. The agent may use a C or B instruction to remove an obstacle immediately in front of it, if it is carrying the appropriate tool. A tree may be removed with a C (chop) instruction, if an axe is held. A wall or tree may be removed with a B (blast) instruction, if dynamite is held.

If the agent moves forward into the water, it will drown unless it is in a boat. Boats behave a bit differently from the other tools, because they always remain in the water. When the agent moves from the water back to the land, it automatically drops the boat. The boat will then remain where it was dropped, ready to be picked up again at a later time.

If the agent attempts to move off the edge of the environment, it dies.


# To Run the Game #

To run the game with the computer agent:

1. Open 2 windows and cd to ai-path-search directory (Or to original_game directory if you want to play the game yourself)

2. Choose a port number between 1025 and 65535 - let's suppose you choose 31415.

3. Type in one window:

```
#!shell

javac *.java
java Bounty -p 31415 -i test_files/s0.in
#Or if you're playing the game yourself
#java Bounty -p 31415 -i ../test-_files/s0.in
```

4.Type into the other window

```
#!shell

java Agent -p 31415
```
/*********************************************
 * Min Jee Son z3330687
 * COMP3411 Artificial Intelligence
 */

/*
 * In the program, the Agent scans the given view and copies it into its own larger map. Every time a new view input is given,
 * the Agent uses its current direction and its previous action to determine if the input depicts any area not seen previously.
 * If it determines that no new areas are being shown, the Agent continues with its plan made in the previous get_action call. 
 * Otherwise the Agent updates its own copy to include the new area and determines the best plan to get to a goal location. 
 * This goal location is dependent on the Agent's virtual map. The gold location has a precedence as a goal location. If this is not 
 * yet known, or if there are no direct ways of getting there, the Agent then looks for the closest known and accessible axe or 
 * dynamite locations, then followed by locations yet unknown.
 * The program uses A*Star search with Total Manhattan Distance Heuristics and uses nested State class to emulate possible future states
 * of the Agent based on its current knowledge of the environment.
 */

import java.util.*;
import java.io.*;
import java.net.*;

public class Agent {
	final static int NORTH = 0;
	final static int EAST = 1;
	final static int SOUTH = 2;
	final static int WEST = 3;
	final static char UNKNOWN = '?';
	final static char OFFMAP = '.';
	final static int initPosX = 79;
	final static int initPosY = 79;

	private boolean have_axe;
	private boolean have_gold;
	private boolean in_boat;
	private int num_dynamites_held;
	private int currDirection;
	private int currPosX;
	private int currPosY;
	private int prevPosX;
	private int prevPosY;
	private char[][] map;
	private char prevAction;
	private int northEdge;
	private int southEdge;
	private int eastEdge;
	private int westEdge;
	private LinkedList<int[]> dynamiteLocations;
	private LinkedList<int[]> boatLocations;
	private LinkedList<int[]> axeLocations;
	private int[] goldLocation;
	private LinkedList<Character> plan;
	private int space;

	public Agent() {
		have_axe = false;
		have_gold = false;
		in_boat = false;
		space = 2;
		num_dynamites_held = 0;
		map = new char[160][160];
		currDirection = NORTH;
		currPosX = initPosX;
		currPosY = initPosY;
		prevAction = UNKNOWN;
		goldLocation = null;
		dynamiteLocations = new LinkedList<>();
		boatLocations = new LinkedList<>();
		axeLocations = new LinkedList<>();
		for (int i = 0; i < 160; i++) {
			for (int j = 0; j < 160; j++) {
				map[i][j] = UNKNOWN;
			}
		}
		plan = null;
		northEdge = 0;
		southEdge = 159;
		eastEdge = 159;
		westEdge = 0;
	}

	// return a list of new areas found
	public HashMap<int[], Character> updateMap(char view[][]) {
		int viewLength = 5;
		HashMap<int[], Character> newAreas = new HashMap<>();
		int yEdge;
		int xEdge;

		char currSpace = map[currPosY][currPosX];
		if(currSpace == 'a'){
			map[currPosY][currPosX] = ' ';
			have_axe = true;
			for(int[] l : axeLocations){
				if(l[0] == currPosY && l[1] == currPosX){
					axeLocations.remove(l);
					break;
				}
			}
		}else if(currSpace == 'd'){
			map[currPosY][currPosX] = ' ';
			num_dynamites_held++;
			for(int[] l : dynamiteLocations){
				if(l[0] == currPosY && l[1] == currPosX){
					dynamiteLocations.remove(l);
					break;
				}
			}
		}else if(currSpace == 'B'){
			map[currPosY][currPosX] = '~';
			in_boat = true;
			for(int[] l : boatLocations){
				if(l[0] == currPosY && l[1] == currPosX){
					boatLocations.remove(l);
					break;
				}
			}
		}
		else if(currSpace != '~' && map[prevPosY][prevPosX]=='~'){
			map[prevPosY][prevPosX] = 'B';
			in_boat = false;
			int [] prevSpace = {prevPosY, prevPosX};
			boatLocations.add(prevSpace);
		}else if(currSpace == 'g'){
			have_gold = true;
		}
		if (prevAction == UNKNOWN) {
			for (int i = - space; i <= space; i++) {
				for (int j = -space; j <= space; j++) {
					int y = currPosY + i;
					int x = currPosX + j;
					if(i==0 && j==0) map[y][x] = ' ';
					else map[y][x] = view[i+space][j+space];
					if(i==0 && j == -space && view[i+space][j+space]==OFFMAP) westEdge = x;
					else if (i==-space && j == 0 && view[i+space][j+space]==OFFMAP) northEdge = y;
					else if (i==0 && j == space && view[i+space][j+space]==OFFMAP) eastEdge = x;
					else if (i==space && j == space && view[i+space][j+space]==OFFMAP) southEdge = y;
					else{
						int[] newArea = { y, x };
						newAreas.put(newArea, view[i+space][j+space]);
					}
				}
			}
			return newAreas;

		}else if (prevAction == 'F') {
			if(currDirection == NORTH){
				yEdge = currPosY - space;
				for(int x = 0; x<viewLength; x++){
					xEdge = currPosX - space + x;
					if(map[yEdge][xEdge] == UNKNOWN){
						map[yEdge][xEdge] = view[0][x];
						if(view[0][x]!=OFFMAP){
							int[] newArea = {yEdge, xEdge};
							newAreas.put(newArea, view[0][x]);
						}else if (x == space && view[0][x] == OFFMAP) 
							northEdge = Math.max(northEdge, yEdge);
 					}
				}
			}else if(currDirection == SOUTH){
				yEdge = currPosY+space;
				for(int x = 0; x<viewLength; x++){
					xEdge = currPosX - space + (viewLength-1-x);
					if(map[yEdge][xEdge] == UNKNOWN){
						map[yEdge][xEdge] = view[0][x];
						if(view[0][x]!=OFFMAP){
							int[] newArea = {yEdge, xEdge};
							newAreas.put(newArea, view[0][x]);
						}else if (x == space && view[0][x] == OFFMAP) 
							southEdge = Math.min(southEdge, yEdge);
					}
				}
			}else if (currDirection == EAST) {
				xEdge = currPosX + space;
				for (int x = 0; x < viewLength; x++) {
					yEdge = currPosY - space + x;
					if (map[yEdge][xEdge] == UNKNOWN) {
						map[yEdge][xEdge] = view[0][x];
						if(view[0][x]!=OFFMAP){
							int[] newArea = {yEdge, xEdge};
							newAreas.put(newArea, view[0][x]);
						}else if (x == space && view[0][x] == OFFMAP) 
							eastEdge = Math.min(eastEdge, xEdge);
					}
				}
			}else{
				xEdge = currPosX - space;
				for(int x = 0; x<viewLength; x++){
					yEdge = currPosY - space + (viewLength - 1 - x);
					if (map[yEdge][xEdge] == UNKNOWN) {
						map[yEdge][xEdge] = view[0][x];
						if(view[0][x]!=OFFMAP){
							int[] newArea = {yEdge, xEdge};
							newAreas.put(newArea, view[0][x]);
						}else if (x == space && view[0][x] == OFFMAP) 
							westEdge = Math.max(westEdge, xEdge);
					}
				}
			}
		}
		return newAreas;
	}
	
	public void updateBearing(char currAction){
		prevPosX = currPosX;
		prevPosY = currPosY;
		if (currAction == 'L') {
			currDirection+=3;
			currDirection = currDirection % 4;
		} else if (currAction == 'R') {
			currDirection++;
			currDirection = currDirection % 4;
		} else {
			int tempX = currPosX;
			int tempY = currPosY;
			switch (currDirection) {
			case NORTH:
				tempY--;
				break;
			case SOUTH:
				tempY++;
				break;
			case EAST:
				tempX++;
				break;
			case WEST:
				tempX--;
				break;
			}
			if(currAction == 'F'){
				currPosX = tempX;
				currPosY = tempY;
			}else if(currAction == 'B'){
				map[tempY][tempX] = ' ';
				num_dynamites_held--;
			}else if(currAction == 'C'){
				map[tempY][tempX] = ' ';
			}
		}
	}
	
	public char get_action(char view[][]) {
		char currAction = 'F';
		HashMap<int[], Character> newAreas = updateMap(view);
		PriorityQueue<State> queue = new PriorityQueue<>();
		LinkedList<State> visited = new LinkedList<>();
		LinkedList<int[]> goalLocations = new LinkedList<>();

		if(have_gold){ 
			State initState = new State(initPosY, initPosX);
			State finalState=null;
			queue.add(initState);
			while(queue.size()>0){
				State currState = queue.poll();
				if(currState.reachedGoal()) {
					finalState = currState;
					break;
				}
				if(visited.contains(currState)) continue;
				visited.add(currState);
				for(State s : currState.getChildren()) {
					queue.add(s);
				}
			}
			if(finalState!=null){
				plan = finalState.getPlan();
				if(plan.size()>0){
					currAction = plan.poll();
				    updateBearing(currAction);
				    prevAction = currAction;
				    return currAction;
				}
			}
		}
		/*
		for(int i = 60; i <= 130; i++){
			for(int j = 70; j <= 130; j++){
				if(i==currPosY && j==currPosX) System.out.print('I');
				else System.out.print(map[i][j]);
			}
			System.out.print("\n");
		}
		*/
		// If new areas are not found, continue with current plan
		if (newAreas.size() == 0){
			if(plan.size()>0){
				currAction = plan.poll();
			    updateBearing(currAction);
			    prevAction = currAction;
			    return currAction;
			}
		}
		// If new areas are found, save any tools/Boat found
		for(int[] currLoc : newAreas.keySet()){
			char c = newAreas.get(currLoc);
			if(c == 'g') goldLocation = currLoc;
			else if(c == 'a') axeLocations.add(currLoc);
			else if(c == 'd') dynamiteLocations.add(currLoc);
			else if(c == 'B') boatLocations.add(currLoc);
		}
		
		// Search if a way to gold exists
		State finalState = null;
		if(goldLocation != null){
			State initState = new State(goldLocation[0], goldLocation[1]);
			queue.add(initState);
			while(queue.size()>0){
				State currState = queue.poll();
				if(currState.reachedGoal()) {
					finalState = currState;
					break;
				}
				if(visited.contains(currState)) continue;
				visited.add(currState);
				for(State s : currState.getChildren()) {
					queue.add(s);
				}
			}
			if(finalState!=null){
				plan = finalState.getPlan();
				if(plan.size()>0){
					currAction = plan.poll();
				    updateBearing(currAction);
				    prevAction = currAction;
				    return currAction;
				}
			}
		}
		//Search for the closest axe / dynamite locations
		visited.clear();
		if(!have_axe) goalLocations.addAll(axeLocations);
		goalLocations.addAll(dynamiteLocations);
		for(int[] loc : goalLocations){
			queue.add(new State(loc[0], loc[1]));
		}
		while(queue.size()>0){
			State currState = queue.poll();
			if(currState.reachedGoal()) {
				finalState = currState;
				break;
			}
			if(visited.contains(currState)) continue;
			visited.add(currState);
			for(State s : currState.getChildren()) {
				queue.add(s);
			}
		}
		if(finalState!= null){
			plan = finalState.getPlan();
			if(plan.size()>0){
				currAction = plan.poll();
			    updateBearing(currAction);
			    prevAction = currAction;
			    return currAction;
			}
		}
		//Search for the closest unknown locations
		visited.clear();
		outerloop:
		for(int i=1; i < 159; i++){
			int y = currPosY-i;
			for(int x = Math.max(westEdge+space, currPosX-space-i); x <= Math.min(eastEdge-space, currPosX+space+i); x++ ){
				if(y<=northEdge) break;
				if( (map[y][x]=='T' && !have_axe) 
						|| map[y][x]=='*' || map[y][x] == UNKNOWN || map[y][x] == OFFMAP) continue;
				for(int viewX = x-space; viewX<=x+space;viewX++){
					for(int viewY = y-space; viewY<=y; viewY++){
					if(map[viewY][viewX]==UNKNOWN){
						State newState = new State(y,x);
						if(!queue.contains(newState)) 
							queue.add(new State(y,x)); 
						if(queue.size()>300) break outerloop;
						break;
					}
					}
				}
			}
			y = currPosY+i;
			for(int x = Math.max(westEdge+space, currPosX-space-i); x <= Math.min(eastEdge-space, currPosX+space+i); x++){
				if(y>=southEdge) break;
				if((map[y][x]=='T' && !have_axe) 
						|| map[y][x]=='*' || map[y][x] == UNKNOWN || map[y][x] == OFFMAP) continue;
				for(int viewX = x-space; viewX<=x+space;viewX++){
					for(int viewY = y; viewY<=y+space; viewY++){
					if(map[viewY][viewX] == UNKNOWN){
						State newState = new State(y,x);
						if(!queue.contains(newState)) 
							queue.add(new State(y,x)); 
						if(queue.size()>300) break outerloop;
						break;
					}
					}
				}
			}
			int x = currPosX-i;
			for(y = Math.max(northEdge, currPosY-space-i); y<=Math.min(southEdge, currPosY+space+i); y++){
				if(x<=westEdge) break;
				if( (map[y][x]=='T' && !have_axe) 
						|| map[y][x]=='*' || map[y][x] == UNKNOWN || map[y][x] == OFFMAP) continue;
				for(int viewY = y-space; viewY <=y+space; viewY++){
					for(int viewX = x-space;viewX<=x;viewX++){
					if(map[viewY][viewX] == UNKNOWN){
						State newState = new State(y,x);
						if(!queue.contains(newState)) 
							queue.add(new State(y,x)); 
						if(queue.size()>300) break outerloop;
						break;
					}
					}
				}
			}
			x = currPosX+i;
			for(y = Math.max(northEdge, currPosY-space-i); y<=Math.min(southEdge, currPosY+space+i); y++){
				if(x>=eastEdge) break;
				if((map[y][x]=='T' && !have_axe) 
						|| map[y][x]=='*' || map[y][x] == UNKNOWN || map[y][x] == OFFMAP) continue;
				for(int viewY = y-space; viewY <=y+space; viewY++){
					for(int viewX = x;viewX<=x+space;viewX++){
					if(map[viewY][viewX] == UNKNOWN){
						State newState = new State(y,x);
						if(!queue.contains(newState)) 
							queue.add(new State(y,x)); 
						if(queue.size()>300) break outerloop;
						break;
					}
					}
				}
			}
		}
		while(queue.size()>0){
			State currState = queue.poll();
			if(currState.reachedGoal()) {
				finalState = currState;
				break;
			}
			if(visited.contains(currState)) continue;
			visited.add(currState);
			for(State s : currState.getChildren()) {
				queue.add(s);
			}
		}
		if(finalState!= null){ plan = finalState.getPlan();
			currAction = plan.poll();
		    updateBearing(currAction);
		    prevAction = currAction;
		    return currAction;
		}
		System.exit(0);
		return 'F';
	}

	void print_view(char view[][]) {
		int i, j;

		System.out.println("\n+-----+");
		for (i = 0; i < 5; i++) {
			System.out.print("|");
			for (j = 0; j < 5; j++) {
				if ((i == 2) && (j == 2)) {
					System.out.print('^');
				} else {
					System.out.print(view[i][j]);
				}
			}
			System.out.println("|");
		}
		System.out.println("+-----+");
	}

	public static void main(String[] args) {
		InputStream in = null;
		OutputStream out = null;
		Socket socket = null;
		Agent agent = new Agent();
		char view[][] = new char[5][5];
		char action = 'F';
		int port;
		int ch;
		int i, j;

		if (args.length < 2) {
			System.out.println("Usage: java Agent -p <port>\n");
			System.exit(-1);
		}

		port = Integer.parseInt(args[1]);

		try { // open socket to Game Engine
			socket = new Socket("localhost", port);
			in = socket.getInputStream();
			out = socket.getOutputStream();
		} catch (IOException e) {
			System.out.println("Could not bind to port: " + port);
			System.exit(-1);
		}

		try { // scan 5-by-5 window around current location
			while (true) {
				for (i = 0; i < 5; i++) {
					for (j = 0; j < 5; j++) {
						if (!((i == 2) && (j == 2))) {
							ch = in.read();
							if (ch == -1) {
								System.exit(-1);
							}
							view[i][j] = (char) ch;
						}
					}
				}
				agent.print_view(view); // COMMENT THIS OUT BEFORE SUBMISSION
				action = agent.get_action(view);
				out.write(action);
			}
		} catch (IOException e) {
			System.out.println("Lost connection to port: " + port);
			System.exit(-1);
		} finally {
			try {
				socket.close();
			} catch (IOException e) {
			}
		}
	}

	public class State implements Comparable<Object>{
		int locationY;
		int locationX;
		int goalY;
		int goalX;
		int direction;
		State prevState;
		int pathCost;
		LinkedList<Character> connectingMoves;
		private boolean hasAxe;
		private boolean inBoat;
		private int noOfDyn;
		char[][] stateMap;
		HashSet<int[]> spacesCleared;

		public State(int goalY, int goalX) {
			prevState = null;
			pathCost = 0;
			locationY = currPosY;
			locationX = currPosX;
			direction = currDirection;
			connectingMoves = null;
			hasAxe = have_axe;
			inBoat = in_boat;
			noOfDyn = num_dynamites_held;
			this.goalX=goalX;
			this.goalY=goalY;
			stateMap = map.clone();
			connectingMoves = new LinkedList<>();
			spacesCleared = new HashSet<>();
			
		}
		public State(State prevState, int y, int x){
			this.prevState = prevState;
			locationY = y;
			locationX = x;
			connectingMoves = new LinkedList<Character>();
			spacesCleared = prevState.spacesCleared;
			int dirOffSet;
			hasAxe = prevState.hasAxe;
			inBoat = prevState.inBoat;
			noOfDyn = prevState.noOfDyn;
			pathCost = prevState.pathCost;
			goalY = prevState.goalY;
			goalX = prevState.goalX;
			stateMap = prevState.stateMap;
			if(locationY-prevState.getLocationY()==-1) direction = NORTH;
			else if(locationY-prevState.getLocationY()==1) direction = SOUTH;
			else if(locationX-prevState.getLocationX()==-1) direction = WEST;
			else direction = EAST;
			connectingMoves.add('F');
			pathCost++;
			if(stateMap[y][x] == 'T' && hasAxe){
				connectingMoves.addFirst('C');
				pathCost++;
			}else if(stateMap[y][x] == '*' || stateMap[y][x] == 'T'){
				char[][] tempMap = new char[160][160];
				for(int i = 0; i < 160; i ++){
					for(int j = 0; j < 160; j++){
						tempMap[i][j] = stateMap[i][j];
					}
				}
				stateMap = tempMap;
				stateMap[y][x] = ' ';
				connectingMoves.addFirst('B');
				noOfDyn--;
				pathCost++;
				spacesCleared = (HashSet<int[]>) spacesCleared.clone();
				int[] e = { y, x };
				spacesCleared.add(e);
			}else if(stateMap[y][x] == 'a'){
				hasAxe = true;
			}
			else if(stateMap[y][x] == 'd'){ 
				char[][] tempMap = new char[160][160];
				for(int i = 0; i < 160; i ++){
					for(int j = 0; j < 160; j++){
						tempMap[i][j] = stateMap[i][j];
					}
				}
				stateMap = tempMap;
				stateMap[y][x] = ' ';
				noOfDyn++;
			}
			else if(stateMap[y][x] == 'B'){ 
				char[][] tempMap = new char[160][160];
				for(int i = 0; i < 160; i ++){
					for(int j = 0; j < 160; j++){
						tempMap[i][j] = stateMap[i][j];
					}
				}
				stateMap = tempMap;
				stateMap[y][x] = '~';
				inBoat = true;
			}else if (stateMap[y][x] == ' ' && stateMap[prevState.locationY][prevState.locationX] == '~'){
				char[][] tempMap = new char[160][160];
				for(int i = 0; i < 160; i ++){
					for(int j = 0; j < 160; j++){
						tempMap[i][j] = stateMap[i][j];
					}
				}
				stateMap = tempMap;
				stateMap[prevState.locationY][prevState.locationX] = 'B';
				inBoat = false;
			}
			dirOffSet = (direction - prevState.getDirection()+4)%4;
			if(dirOffSet == 3){ 
				connectingMoves.addFirst('L'); 
				pathCost++;
			}
			else 
				for(int i = 0; i<dirOffSet; i++) {
					connectingMoves.addFirst('R');
					pathCost++;
				}	
		}

		public int getLocationX() {
			return locationX;
		}

		public int getLocationY() {
			return locationY;
		}
		public int getFunctionCost(){
			//heuristic = total Manhattan distance
			return pathCost+Math.abs(goalX-locationX)+Math.abs(goalY-locationY);
		}
		public int getDirection(){
			return direction;
		}
		public boolean reachedGoal(){
			return (locationY == goalY && locationX == goalX);
		}
		@Override
		public int compareTo(Object o) {
			State s = (State) o;
			return getFunctionCost() - s.getFunctionCost();
		}
		public LinkedList<State> getChildren(){
			LinkedList<State> children = new LinkedList<>();
			if(movePossible(locationY+1, locationX)) children.add(new State(this, locationY+1, locationX));
			if(movePossible(locationY-1, locationX)) children.add(new State(this, locationY-1, locationX));
			if(movePossible(locationY, locationX+1)) children.add(new State(this, locationY, locationX+1));
			if(movePossible(locationY , locationX-1)) children.add(new State(this, locationY, locationX-1));		
			return children;
		}
		private boolean movePossible(int newY, int newX){
			if(stateMap[newY][newX] == '*' && noOfDyn==0) return false;
			if(stateMap[newY][newX] == '*' && (goldLocation==null || goldLocation[0]!=goalY || goldLocation[1]!=goalX)) return false;
			if(stateMap[newY][newX] == 'T' && !hasAxe && noOfDyn==0) return false;
			if(stateMap[newY][newX] == 'T' && !hasAxe && noOfDyn > 0 && (goldLocation==null || goldLocation[0]!=goalY || goldLocation[1]!=goalX)) return false;
			if((stateMap[newY][newX]=='~' && !inBoat)||stateMap[newY][newX]==UNKNOWN || stateMap[newY][newX]==OFFMAP) return false;
			return true;
		}
		public LinkedList<Character> getPlan(){
			State currState = prevState;
			LinkedList<Character> plan = connectingMoves;
			while(currState!=null){
				plan.addAll(0, currState.connectingMoves);
				currState = currState.prevState;
			}
			return plan;
		}
		
		public boolean equals(Object o){
			if(o.getClass()!=this.getClass()) return false;
			State s = (State)o;
			return (goalX==s.goalX && goalY == s.goalY && locationX==s.locationX && locationY==s.locationY 
					&& hasAxe==s.hasAxe && noOfDyn==s.noOfDyn && spacesCleared.equals(s.spacesCleared));
		}

	}
}